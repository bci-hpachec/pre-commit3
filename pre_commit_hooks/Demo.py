"""
-- CHEQUEO DE FUNCIONES-
"""
#import sys
#sys.path.append("./Clases")
from pre_commit_hooks.query_formatops_v1 import ruta_sqls
#from clase_de_chequeo import *
#from clase_de_lectura import *
from pre_commit_hooks.orquestador_check_query import orquestador_check_query 
from pre_commit_hooks.generador_log import generador_log_bteq


#queries = ruta_sqls('./activo_ejemplo/BTEQs')
queries = ruta_sqls('C:/Users/hpachec/Desktop/Chequeo_bteq/check_normalizacion/QueryOps/activo_ejemplo/BTEQs')
print(queries)

for path_query in queries:
    #path_query = queries[0]
    rslt_orq = orquestador_check_query(path_query)
    sql_evaluado = rslt_orq.let_it_rip()
    generador_log_bteq(sql_evaluado, path_query)
